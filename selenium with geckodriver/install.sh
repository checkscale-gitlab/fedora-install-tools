#!/bin/sh

dnf install -y firefox
pip install selenium
wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
tar -xvf geckodriver-v0.30.0-linux64.tar.gz
mv geckodriver /usr/bin/
chmod +x /usr/bin/geckodriver
